# Echo client program
import socket

HOST = 'localhost'      # The remote host
PORT = 4567             # The same port as used by the server
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))
s.send("""GET / HTTP/1.1
Host: 192.168.0.48:4567
Connection: Upgrade
Pragma: no-cache
Cache-Control: no-cache
Upgrade: websocket
Origin: http://127.0.0.1:8080
Sec-WebSocket-Version: 13
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36
Accept-Encoding: gzip, deflate
Accept-Language: es-ES,es;q=0.9
Sec-WebSocket-Key: vqrOApI2nmiYWV3jsLRu0g==
Sec-WebSocket-Extensions: permessage-deflate; client_max_window_bits""")
s.send("Hola")
data = s.recv(1024)
s.close()
print 'Received', repr(data)