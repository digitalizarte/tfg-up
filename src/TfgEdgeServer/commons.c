#include "commons.h"

char *getMemory(const char *token, int length)
{
    char *temp = (char *)malloc(length);

    if (temp == NULL)
    {
        return NULL;
    }

    memset(temp, '\0', length);
    memcpy(temp, token, length);
    return temp;
}