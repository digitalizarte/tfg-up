#ifndef PROTOCOL
#define PROTOCOL

#include <stdint.h>
#include "commons.h"
#include <RF24Mesh/RF24Mesh.h>

typedef unsigned char BYTE;
typedef char *STRING;

/** 
// #define max(a, b) \
//     ({ __typeof__ (a) _a = (a); \
//        __typeof__ (b) _b = (b); \
//      _a > _b ? _a : _b; })
*/
#define max(a, b) (a > b ? a : b)

/** 
// #define min(a, b) \
//     ({ __typeof__ (a) _a = (a); \
//        __typeof__ (b) _b = (b); \
//      _a < _b ? _a : _b; })
*/
#define min(a, b) (a < b ? a : b)

#define NAME_LEN 32
#define DESCRIPTION_LEN 128
#define ORDERBY_LEN 32
#define FILTER_LEN 128
#define RESOURCE_LEN 32
#define SELECT_LEN 128
#define RESPONSE_LEN 128
#define MEASUREMENT_DATA_LEN 128

// P: Presentación - Mensaje de presentación del dispositivo y sus caracteristicas.
typedef struct
{
    BYTE nodeID;                       // Identificador unico del nodo.
    char name[NAME_LEN];               // Nombre del nodo.
    char description[DESCRIPTION_LEN]; // Descripción del nodo.
    BYTE characteristics;              // Cantidad de sensores y/o actuadores
} payload_P;

payload_P *payload_P_new();
bool presentation(RF24Mesh &mesh, STRING name, STRING description);

// C: Caracteristica - Mensaje donde un nodo envia una de las caracteristicas y/o funcionalidades que ofrece.
typedef struct
{
    BYTE nodeID;                       // Identificador unico del nodo.
    BYTE ID;                           // Identificador de la caracteristica.
    char name[NAME_LEN];               //	Nombre de la caracteristica.
    char description[DESCRIPTION_LEN]; // Descripción de la caracteristica.
    unsigned int interval;             // Intervalo entre mediciónes.
} payload_C;

payload_C *payload_C_new(BYTE nodeID, STRING name, STRING description, BYTE ID, unsigned int interval);
bool characteristic(RF24Mesh &mesh, STRING name, STRING description, BYTE ID, unsigned int interval);

// R: Reconexión - Mensaje  del nodo master donde solicita a un nodo que se vuelva a identificar si no lo encuantra registrado.
typedef struct
{
    BYTE nodeID; // Identificador unico del nodo.
} payload_R;

payload_R *payload_R_new(BYTE nodeID);
bool reconnection(RF24Mesh &mesh);

// E: Echo - Mensaje del nodo master cada determinado tiempo envia un ping a lo nodos para verificar si se encuentran activos.
// Un nodo puede enviar al nodo master un ping para verificar el estado de la conexión."
typedef struct
{
    BYTE nodeID;        // Identificador unico del nodo.
    unsigned long data; // Dato a responder
} payload_E;

payload_E *payload_E_new(BYTE nodeID, unsigned long data);
bool echo(RF24Mesh &mesh, unsigned long data, BYTE toNodeID = 0);

// M: Medición - Mensaje que envia un nodo al nodo master indicando que se ha obtenido el valor de una medición de alguna de sus caracteristicas.
typedef struct
{
    BYTE nodeID;                     // Identificador unico del nodo.
    BYTE ID;                         // Identificador de la caracteristica.
    char data[MEASUREMENT_DATA_LEN]; // Dato a enviar.
    BYTE type;                       // Tipo de dato.
} payload_M;

payload_M *payload_M_new(BYTE nodeID, BYTE ID, STRING data, BYTE type);
bool measurement(RF24Mesh &mesh, BYTE ID, STRING data, BYTE type);
bool measurement(RF24Mesh &mesh, BYTE ID, STRING data);
bool measurement(RF24Mesh &mesh, BYTE ID, BYTE data);
bool measurement(RF24Mesh &mesh, BYTE ID, unsigned int data);
bool measurement(RF24Mesh &mesh, BYTE ID, int data);
bool measurement(RF24Mesh &mesh, BYTE ID, unsigned long data);
bool measurement(RF24Mesh &mesh, BYTE ID, long data);
bool measurement(RF24Mesh &mesh, BYTE ID, float data);

// 'Q' : Query
typedef struct
{
    BYTE nodeID;                 // Identificador unico del nodo.
    char resource[RESOURCE_LEN]; // Recurso que se esta consultando.
    char orderby[ORDERBY_LEN];   // Orden en el cual se esperan los resultados
    unsigned long top;           // Tomar los primeros X registros.
    unsigned long skip;          // Saltear X cantidad de registros.
    bool inlinecount;            // Devolver la cantidad de registros totales.
    char filter[FILTER_LEN];     // Filtro de la consulta.
    char select[SELECT_LEN];     // Selección de datos de la consulta.
} payload_Q;

payload_Q *payload_Q_new(BYTE nodeID,
                         STRING resource,
                         STRING orderby,
                         unsigned long top,
                         unsigned long skip,
                         bool inlinecount,
                         STRING filter,
                         STRING select);

bool measurement(RF24Mesh &mesh, STRING resource,
                 STRING orderby,
                 unsigned long top,
                 unsigned long skip,
                 bool inlinecount,
                 STRING filter,
                 STRING select);

typedef struct
{
    unsigned long node;
    char resource[RESOURCE_LEN];
    char response[RESPONSE_LEN];
} payload_q_res;

#endif
