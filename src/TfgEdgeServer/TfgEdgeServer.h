#ifndef TFGEDGESERVER
#define TFGEDGESERVER

// Radio
#include <RF24Mesh/RF24Mesh.h>
#include <RF24/RF24.h>
#include <RF24Network/RF24Network.h>
#include <time.h>
#include <json/json.h>

#include "Handshake.h"
#include "Communicate.h"
#include "Errors.h"
#include "protocol.h"

#define PIN_25 25

#define PORT 4567

#define INTERVAL_PING 60000

void *handleRadio(void *arg);
void radio_setup();
void radio_loop();
// bool radio_connect_relay();
// void radio_process_message_r();
void radio_process_message_unknown();
// void radio_process_message_p();
// void radio_process_message_l();
// void radio_process_message_q();
void radio_dump_header(RF24NetworkHeader header);

void radio_process_message_C(void);
void radio_process_message_E(void);
void radio_process_message_M(void);
void radio_process_message_P(void);
void radio_process_message_Q(void);
void radio_process_message_R(void);
void radio_process_message_S(void);

// bool radio_ping();
// void radio_check_ping();
void radio_check_interval();

void ws_send_message(ws_list *l, const char *data);
void demo_node_add(void);
#endif