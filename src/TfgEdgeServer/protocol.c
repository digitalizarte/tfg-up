#include "protocol.h"

payload_P *payload_P_new(BYTE nodeID, STRING name, STRING description)
{
    payload_P *ret = (payload_P *)malloc(sizeof(payload_P));
    if (ret != NULL)
    {
        ret->nodeID = nodeID;
        memset(ret->name, '\0', NAME_LEN);
        memcpy(ret->name, name, min(strlen(name), NAME_LEN - 1));

        memset(ret->description, '\0', DESCRIPTION_LEN);
        memcpy(ret->description, description, min(strlen(description), DESCRIPTION_LEN - 1));
    }

    return ret;
}

bool presentation(RF24Mesh &mesh, STRING name, STRING description)
{
    bool ret;
    BYTE nodeID = mesh.getNodeID();
    payload_P *msg = payload_P_new(nodeID, name, description);
    if (msg != NULL)
    {
        ret = mesh.write(&msg, 'P', sizeof(msg));
        free(msg);
    }
    else
    {
        ret = 0;
    }

    return ret;
}

payload_C *payload_C_new(BYTE nodeID, STRING name, STRING description, BYTE ID, unsigned int interval)
{
    payload_C *ret = (payload_C *)malloc(sizeof(payload_C));
    if (ret != NULL)
    {
        ret->nodeID = nodeID;
        ret->ID = ID;
        ret->interval = interval;
        memset(ret->name, '\0', NAME_LEN);
        memcpy(ret->name, name, min(strlen(name), NAME_LEN - 1));

        memset(ret->description, '\0', DESCRIPTION_LEN);
        memcpy(ret->description, description, min(strlen(description), DESCRIPTION_LEN - 1));
    }

    return ret;
}

bool characteristic(RF24Mesh &mesh, STRING name, STRING description, BYTE ID, unsigned int interval)
{
    bool ret;
    BYTE nodeID = mesh.getNodeID();
    payload_C *msg = payload_C_new(nodeID, name, description, ID, interval);
    if (msg != NULL)
    {
        ret = mesh.write(&msg, 'C', sizeof(msg));
        free(msg);
    }
    else
    {
        ret = 0;
    }

    return ret;
}

payload_R *payload_R_new(BYTE nodeID)
{
    payload_R *ret = (payload_R *)malloc(sizeof(payload_R));
    if (ret != NULL)
    {
        ret->nodeID = nodeID;
    }

    return ret;
}

bool reconnection(RF24Mesh &mesh)
{
    bool ret;
    BYTE nodeID = mesh.getNodeID();
    payload_R *msg = payload_R_new(nodeID);
    if (msg != NULL)
    {
        ret = mesh.write(&msg, 'R', sizeof(msg));
        free(msg);
    }
    else
    {
        ret = 0;
    }

    return ret;
}

payload_E *payload_E_new(BYTE nodeID, unsigned long data)
{
    payload_E *ret = (payload_E *)malloc(sizeof(payload_E));
    if (ret != NULL)
    {
        ret->nodeID = nodeID;
        ret->data = data;
    }

    return ret;
}

bool echo(RF24Mesh &mesh, unsigned long data, BYTE toNodeID)
{
    bool ret;
    BYTE nodeID = mesh.getNodeID();
    payload_E *msg = payload_E_new(nodeID, data);
    if (msg != NULL)
    {
        ret = mesh.write(&msg, 'E', sizeof(msg), toNodeID);
        free(msg);
    }
    else
    {
        ret = 0;
    }

    return ret;
}

payload_M *payload_M_new(BYTE nodeID, BYTE ID, STRING data, BYTE type)
{
    payload_M *ret = (payload_M *)malloc(sizeof(payload_M));
    if (ret != NULL)
    {
        ret->nodeID = nodeID;
        ret->ID = ID;
        ret->type = type;
        memset(ret->data, '\0', MEASUREMENT_DATA_LEN);
        memcpy(ret->data, data, min(strlen(data), MEASUREMENT_DATA_LEN - 1));
    }

    return ret;
}

bool measurement(RF24Mesh &mesh, BYTE ID, STRING data, BYTE type)
{
    bool ret;
    BYTE nodeID = mesh.getNodeID();
    payload_M *msg = payload_M_new(nodeID, ID, data, type);
    if (msg != NULL)
    {
        ret = mesh.write(&msg, 'M', sizeof(msg));
        free(msg);
    }
    else
    {
        ret = 0;
    }

    return ret;
}

bool measurement(RF24Mesh &mesh, BYTE ID, STRING data)
{
    return measurement(mesh, ID, data, 1);
}

bool measurement(RF24Mesh &mesh, BYTE ID, BYTE data)
{
    bool ret;
    char new_data[MEASUREMENT_DATA_LEN];
    memset(new_data, '\0', MEASUREMENT_DATA_LEN);
    sprintf(new_data, "%d", data);
    ret = measurement(mesh, ID, new_data, 1);
    return ret;
}

bool measurement(RF24Mesh &mesh, BYTE ID, unsigned int data)
{
    bool ret;
    char new_data[MEASUREMENT_DATA_LEN];
    memset(new_data, '\0', MEASUREMENT_DATA_LEN);
    sprintf(new_data, "%d", data);
    ret = measurement(mesh, ID, new_data, 1);
    return ret;
}

bool measurement(RF24Mesh &mesh, BYTE ID, int data)
{
    bool ret;
    char new_data[MEASUREMENT_DATA_LEN];
    memset(new_data, '\0', MEASUREMENT_DATA_LEN);
    sprintf(new_data, "%d", data);
    ret = measurement(mesh, ID, new_data, 1);
    return ret;
}

bool measurement(RF24Mesh &mesh, BYTE ID, unsigned long data)
{
    bool ret;
    char new_data[MEASUREMENT_DATA_LEN];
    memset(new_data, '\0', MEASUREMENT_DATA_LEN);
    sprintf(new_data, "%lu", data);
    ret = measurement(mesh, ID, new_data, 1);
    return ret;
}

bool measurement(RF24Mesh &mesh, BYTE ID, long data)
{
    bool ret;
    char new_data[MEASUREMENT_DATA_LEN];
    memset(new_data, '\0', MEASUREMENT_DATA_LEN);
    sprintf(new_data, "%ld", data);
    ret = measurement(mesh, ID, new_data, 1);
    return ret;
}

bool measurement(RF24Mesh &mesh, BYTE ID, float data)
{
    bool ret;
    char new_data[MEASUREMENT_DATA_LEN];
    memset(new_data, '\0', MEASUREMENT_DATA_LEN);
    sprintf(new_data, "%f", data);
    ret = measurement(mesh, ID, new_data, 1);
    return ret;
}

payload_Q *payload_Q_new(BYTE nodeID,
                         STRING resource,
                         STRING orderby,
                         unsigned long top,
                         unsigned long skip,
                         bool inlinecount,
                         STRING filter,
                         STRING select)
{
    payload_Q *ret = (payload_Q *)malloc(sizeof(payload_Q));
    if (ret != NULL)
    {
        ret->nodeID = nodeID;
        memset(ret->resource, '\0', RESOURCE_LEN);
        memcpy(ret->resource, resource, min(strlen(resource), RESOURCE_LEN - 1));

        memset(ret->orderby, '\0', ORDERBY_LEN);
        memcpy(ret->orderby, orderby, min(strlen(orderby), ORDERBY_LEN - 1));

        ret->top = top;
        ret->skip = skip;
        ret->inlinecount = inlinecount;

        memset(ret->filter, '\0', FILTER_LEN);
        memcpy(ret->filter, filter, min(strlen(filter), FILTER_LEN - 1));

        memset(ret->select, '\0', SELECT_LEN);
        memcpy(ret->select, select, min(strlen(select), SELECT_LEN - 1));
    }

    return ret;
}

bool query(RF24Mesh &mesh,
           STRING resource,
           STRING orderby,
           unsigned long top,
           unsigned long skip,
           bool inlinecount,
           STRING filter,
           STRING select)
{
    bool ret;
    BYTE nodeID = mesh.getNodeID();
    payload_Q *msg = payload_Q_new(nodeID, resource, orderby, top, skip, inlinecount, filter, select);
    if (msg != NULL)
    {
        ret = mesh.write(&msg, 'Q', sizeof(msg));
        free(msg);
    }
    else
    {
        ret = 0;
    }

    return ret;
}